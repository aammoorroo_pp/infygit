import 'materialize-css/dist/css/materialize.min.css';

import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

import React, { Component } from "react";
import { render } from "react-dom";

import "./style.css";

render(<App />, document.getElementById("root"));
registerServiceWorker();
